# TestFigure-in-Java




public class TestGeoFigure {

 public static void main(String[] args){
  //Create an array of type GeoFigure
  GeoFigure gF[] = {new Triangle(10, 25), new Circle(5),new Rectangle(10, 20),new Rectangle(30, 30) };
  
  //Compute area of each figure and it to total
  // Display each figure and its area
  double totalArea = 0;
  for(int i = 0; i < gF.length; i++){
     double area = gf[i].computeArea();
     totalArea += area;
     System.out.println(gF[i].toString());
     System.out.println("Area is " + area + "\n");
     
   }
  
   System.out.println("\nTotal are is " + totalArea);
  }
 }     
